// ==UserScript==
// @name         New Mode Auto Counter
// @version      1.4.3
// @description  Contador automático de chances do time para o modo novo.
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @grant        none
// @require      https://raw.githubusercontent.com/TheJoseph98/AMQ-Scripts/master/common/amqWindows.js
// @require      https://raw.githubusercontent.com/TheJoseph98/AMQ-Scripts/master/common/amqScriptInfo.js
// @require      https://github.com/amq-script-project/AMQ-Scripts/raw/master/gameplay/amqAnswerTimesUtility.user.js
// @updateURL    https://gitlab.com/amq-pscid/new-mode-auto-counter/-/raw/master/New%20Mode%20Auto%20Counter.user.js
// ==/UserScript==


if (document.getElementById("startPage")) return;

var initialChances = 5;
let editCommand = "/vidas";
var autoIsOn = false;

function toggleAuto() {
    if (checkCurrentMode()) {
        autoIsOn = autoIsOn ? false : true;
        if (autoIsOn) {
            gameChat.systemMessage("Contador ligado!");
        } else {
            gameChat.systemMessage("Contador desligado!");
        }
        document.getElementById("chancesMinusBtn").disabled = !autoIsOn;
        document.getElementById("chancesPlusBtn").disabled = !autoIsOn;
        document.getElementById("chancesResetBtn").disabled = !autoIsOn;
    } else {
        gameChat.systemMessage("Contador apenas no modo de times!");
    }
}

function sendChances() {
    if (document.getElementById("gcTeamChatSwitch").classList.contains("active")) {
        document.getElementById('gcTeamChatSwitch')
            .dispatchEvent(new MouseEvent('click', {shiftKey: false}));
    }
    var chancesToSend = "";
    var retrievedObject = localStorage.getItem('newModeData');
    var newModeDataObject = JSON.parse(retrievedObject);
    var input = document.getElementById("gcInput");
    for (var i = 0; i < newModeDataObject.chances.length; ++i) {
         chancesToSend = chancesToSend + newModeDataObject.chances[i] + " - ";
    }
    input.value = chancesToSend.substring(0, chancesToSend.length - 3);
    var ev = document.createEvent('Event');
    ev.initEvent('keypress');
    ev.which = ev.keyCode = 13;
    input.dispatchEvent(ev);
}

function editChances(index) {
    var retrievedObject = localStorage.getItem('newModeData');
    var newModeDataObject = JSON.parse(retrievedObject);
    var oldNumber = newModeDataObject.chances[index];
    var newNumber = (parseInt(oldNumber) - 1);
    if (newNumber < 0) {
        autoIsOn = false;
        document.getElementById("chancesMinusBtn").disabled = true;
        document.getElementById("chancesPlusBtn").disabled = true;
        document.getElementById("chancesResetBtn").disabled = true;
        resetChances();
        gameChat.systemMessage("Alguém com 0 vidas deu a resposta, desligando contador...");
    } else {
        var isResetNeeded = true;
        newModeDataObject.chances[index] = newNumber;
        for (var i = 0; i < newModeDataObject.chances.length; ++i) {
            if (newModeDataObject.chances[i] > 0) {
                isResetNeeded = false;
            }
        }
        if (isResetNeeded) {
            for (var j = 0; j < newModeDataObject.chances.length; ++j) {
                newModeDataObject.chances[j] = initialChances;
            }
        }
        localStorage.setItem('newModeData', JSON.stringify(newModeDataObject));
    }
}

function forceEditChances(type) {
    var retrievedObject = localStorage.getItem('newModeData');
    var newModeDataObject = JSON.parse(retrievedObject);
    for (var i = 0; i < newModeDataObject.chances.length; ++i) {
        var oldNumber = newModeDataObject.chances[i];
        var newNumber;
        if (type == "plus") {
            newNumber = (parseInt(oldNumber) + 1);
        } else {
            newNumber = (parseInt(oldNumber) - 1);
        }
        if (newNumber > 0) {
            newModeDataObject.chances[i] = newNumber;
        }
    }
    localStorage.setItem('newModeData', JSON.stringify(newModeDataObject));
    sendChances();
}

function forceEditChancesCommand(chancesArray) {
    var retrievedObject = localStorage.getItem('newModeData');
    var newModeDataObject = JSON.parse(retrievedObject);
    for (var i = 0; i < chancesArray.length; ++i) {
        var currentChance = parseInt(chancesArray[i]);
        newModeDataObject.chances[i] = currentChance;
    }
    localStorage.setItem('newModeData', JSON.stringify(newModeDataObject));
    sendChances();
}

function resetChances() {
    var retrievedObject = localStorage.getItem('newModeData');
    var newModeDataObject = JSON.parse(retrievedObject);
    for (var i = 0; i < newModeDataObject.chances.length; ++i) {
        newModeDataObject.chances[i] = initialChances;
    }
    localStorage.setItem('newModeData', JSON.stringify(newModeDataObject));
    if (autoIsOn) {
        sendChances();
    }
}

var currentTeam;
var teamPlayers = [];
var chances = [];
var newChances;
var teamCurrentAnswers = new Map();
var newModeData = '{"chances": [], "currentTeam": "0"}';
var teamRoundData = '{"guessTimeArray": [], "playerResultsArray": []}';

function getPlayers() {
    const chancesJson = JSON.parse(newModeData);
    localStorage.removeItem("newModeData");
    for(let player of Object.values(quiz.players)) {
        if (player.isSelf == true) {
            currentTeam = player.teamNumber;
        }
        teamPlayers.push(player.teamNumber + player._name);
    }
    for (var i = 0; i < teamPlayers.length; ++i) {
        if (teamPlayers[i].startsWith(currentTeam)) {
            chances.push(initialChances);
        }
    }
    chancesJson["chances"] = chances;
    chancesJson["currentTeam"] = currentTeam;
    teamPlayers = [];
    chances = [];
    localStorage.setItem("newModeData", JSON.stringify(chancesJson));
}

function checkCurrentMode() {
    if (autoIsOn && quiz.isSpectator) {
        autoIsOn = autoIsOn ? false : true;
        document.getElementById("chancesMinusBtn").disabled = true;
        document.getElementById("chancesPlusBtn").disabled = true;
        document.getElementById("chancesResetBtn").disabled = true;
    }
    return quiz.teamMode && !quiz.isSpectator;
}

// Setup
let loadInterval = setInterval(() => {
    if (document.getElementById("loadingScreen").classList.contains("hidden")) {
        setup();
        clearInterval(loadInterval);
    }
}, 500);

function setup() {
    let quizReadyListener = new Listener("quiz ready", (data) => {
        if (checkCurrentMode()) {
            getPlayers();
        }
    });

    let resultsListener = new Listener("answer results", (result) => {
        if (autoIsOn && checkCurrentMode()) {
            let newSong = {
                name: result.songInfo.songName,
                anime: result.songInfo.animeNames,
                players: Object.values(result.players)
                .map((tmpPlayer) => {
                    let tmpObj = {
                        name: quiz.players[tmpPlayer.gamePlayerId]._name,
                        correct: tmpPlayer.correct,
                        answer: quiz.players[tmpPlayer.gamePlayerId].avatarSlot.$answerContainerText.text(),
                        guessTime: amqAnswerTimesUtility.playerTimes[tmpPlayer.gamePlayerId],
                        gamePlayerId: tmpPlayer.gamePlayerId,
                        teamNumber: quiz.players[tmpPlayer.gamePlayerId].teamNumber
                    };
                    return tmpObj;
                })
            };
            var retrievedObject = localStorage.getItem('newModeData');
            var newModeDataObject = JSON.parse(retrievedObject);
            var teamRetrievedObject = localStorage.getItem('teamRoundData');
            var teamRoundDataObject = JSON.parse(teamRetrievedObject, reviver);
            let currentPlayerChancesIndex = 0;
            let playerChancesIndex;
            var lastGuessTime;
            if (teamRoundDataObject.size != 0) {
                for (let playerOrdered of Object.values(quiz.players)) {
                    for (let player of newSong.players) {
                        if (playerOrdered.name == player.name && player.correct == true && player.teamNumber == newModeDataObject.currentTeam) {
                            if (teamRoundDataObject.has(player.gamePlayerId)) {
                                var currentPlayerResults = teamRoundDataObject.get(player.gamePlayerId);
                                for (var i = 0; i < currentPlayerResults.guessTimeArray.length; ++i) {
                                    if (typeof player.guessTime != 'undefined' && (typeof lastGuessTime == 'undefined' || lastGuessTime > currentPlayerResults.guessTimeArray[i])) {
                                        if ((currentPlayerResults.playerResultsArray[i].toLowerCase() == player.answer.toLowerCase()
                                             || currentPlayerResults.playerResultsArray[i].toLowerCase() == newSong.anime.english.toLowerCase()
                                             || currentPlayerResults.playerResultsArray[i].toLowerCase() == newSong.anime.romaji.toLowerCase())) {
                                            lastGuessTime = currentPlayerResults.guessTimeArray[i];
                                            playerChancesIndex = currentPlayerChancesIndex;
                                        }
                                    }
                                }
                            }
                            currentPlayerChancesIndex++;
                        }
                    }
                }
                editChances(playerChancesIndex);
                if (autoIsOn) {
                    sendChances();
                }
                teamCurrentAnswers.clear();
                localStorage.removeItem("teamRoundData");
            }
        }
    });

    let teamMemberListener = new Listener("team member answer", (result) => {
        const teamRoundJson = JSON.parse(teamRoundData);
        var guessTimeArray = [];
        var playerResultsArray = [];
        var playerGuessTime = amqAnswerTimesUtility.playerTimes[result.gamePlayerId];
        if (typeof playerGuessTime == 'undefined') {
            var teamRetrievedObject = localStorage.getItem('teamRoundData');
            var teamRoundDataObject = JSON.parse(teamRetrievedObject, reviver);
            if (teamRetrievedObject != null) {
                playerGuessTime = 1;
                for (const [key, value] of teamRoundDataObject.entries()) {
                    if (value.guessTimeArray[0] <= 8) {
                        playerGuessTime++;
                    }
                }
            } else {
                playerGuessTime = 1;
            }
        }
        if (teamCurrentAnswers.has(result.gamePlayerId)) {
            guessTimeArray = teamCurrentAnswers.get(result.gamePlayerId).guessTimeArray;
            playerResultsArray = teamCurrentAnswers.get(result.gamePlayerId).playerResultsArray;
        }
        guessTimeArray.push(playerGuessTime);
        playerResultsArray.push(result.answer);
        teamRoundJson["guessTimeArray"] = guessTimeArray;
        teamRoundJson["playerResultsArray"] = playerResultsArray;
        teamCurrentAnswers.set(result.gamePlayerId, teamRoundJson);
        localStorage.setItem("teamRoundData", JSON.stringify(teamCurrentAnswers, replacer));
    });

    let quizOverListener = new Listener("quiz over", (roomSettings) => {
        localStorage.removeItem("newModeData");
        counterWindow.close();
    });

    let quizLeaveListener = new Listener("New Rooms", (rooms) => {
        localStorage.removeItem("newModeData");
    });

    let commandListener = new Listener("game chat update", (payload) => {
        payload.messages.forEach(message => {
            if (message.sender === selfName && message.message.startsWith(editCommand) && checkCurrentMode() && autoIsOn) {
                let args = message.message.split(/\s+/);
                if (args.length == 2 && !isNaN(parseInt(args[1]))) {
                    var retrievedObject = localStorage.getItem('newModeData');
                    var newModeDataObject = JSON.parse(retrievedObject);
                    const chancesArray = args[1].split("-");
                    if (chancesArray.length == newModeDataObject.chances.length) {
                        forceEditChancesCommand(chancesArray);
                    } else {
                        gameChat.systemMessage("Argumento inválido! Ex: 4-3-4-2 para 4 pessoas.");
                    }
                } else {
                    gameChat.systemMessage("Argumento inválido! Ex: 4-3-4-2 para 4 pessoas.");
                }
            }
        });
    });

    quizReadyListener.bindListener();
    resultsListener.bindListener();
    teamMemberListener.bindListener();
    quizOverListener.bindListener();
    quizLeaveListener.bindListener();
    commandListener.bindListener();

    createListWindow();

    $(".modal").on("show.bs.modal", () => {
        counterWindow.setZIndex(1030);
    });

    $(".modal").on("hidden.bs.modal", () => {
        counterWindow.setZIndex(1060);
    });

    $(".slCheckbox label").hover(() => {
        counterWindow.setZIndex(1030);
    }, () => {
        counterWindow.setZIndex(1060);
    });

    AMQ_addStyle(`
        .chancesOptionsButton {
            float: left;
            margin-top: 15px;
            margin-left: 10px;
            padding: 6px 8px;
        }
        #qpNewModeButton {
            width: 30px;
            height: 100%;
            margin-right: 5px;
        }
        #counterAutoStatus {
            padding: 5px;
            margin-right: 5px;
        }
    `);
}

function replacer(key, value) {
  if(value instanceof Map) {
    return {
      dataType: 'Map',
      value: Array.from(value.entries()),
    };
  } else {
    return value;
  }
}

function reviver(key, value) {
  if(typeof value === 'object' && value !== null) {
    if (value.dataType === 'Map') {
      return new Map(value.value);
    }
  }
  return value;
}

let counterWindow;
let counterWindowTable;
let counterWindowOpenButton;

function createListWindow() {
    counterWindow = new AMQWindow({
        title: "Contador",
        width: 190,
        height: 140,
        minWidth: 190,
        minHeight: 140,
        zIndex: 1060,
        resizable: true,
        draggable: true
    });

    counterWindow.addPanel({
        id: "counterWindowOptions",
        width: 1.0,
        height: 65
    });

    counterWindow.panels[0].panel
        .append($(`<button class="btn btn-default chancesOptionsButton" type="button"><i aria-hidden="true" class="fa fa-power-off"></i></button>`)
                .click(() => {
        toggleAuto();
    })
                .popover({
        placement: "bottom",
        content: "Ligar/Desligar",
        trigger: "hover",
        container: "body",
        animation: false
    }))
        .append($(`<button id="chancesMinusBtn" class="btn btn-default chancesOptionsButton" type="button"><i aria-hidden="true" class="fa fa-minus-square"></i></button>`)
                .click(() => {
        if (checkCurrentMode() && autoIsOn) {
            forceEditChances("minus");
        }
    })
                .popover({
        placement: "bottom",
        content: "-1",
        trigger: "hover",
        container: "body",
        animation: false
    })
               )
        .append($(`<button id="chancesPlusBtn" class="btn btn-default chancesOptionsButton" type="button"><i aria-hidden="true" class="fa fa-plus-square"></i></button>`)
                .click(() => {
        if (checkCurrentMode() && autoIsOn) {
            forceEditChances("plus");
        }
    })
                .popover({
        placement: "bottom",
        content: "+1",
        trigger: "hover",
        container: "body",
        animation: false
    })
               )
        .append($(`<button id="chancesResetBtn" class="btn btn-default chancesOptionsButton" type="button"><i aria-hidden="true" class="fa fa-retweet"></i></button>`)
                .click(() => {
        if (checkCurrentMode() && autoIsOn) {
            resetChances();
        }
    })
                .popover({
        placement: "bottom",
        content: "Reset",
        trigger: "hover",
        container: "body",
        animation: false
    })
               );

    counterWindowOpenButton = $(`<div id="qpNewModeButton" class="clickAble qpOption"><i aria-hidden="true" class="fa fa-handshake-o qpMenuItem"></i></div>`)
        .click(function () {
        if(counterWindow.isVisible()) {
            $(".rowSelected").removeClass("rowSelected");
            counterWindow.close();
        }
        else {
            counterWindow.open();
        }
    })
        .popover({
        placement: "bottom",
        content: "Contador",
        trigger: "hover"
    });

    document.getElementById("chancesMinusBtn").disabled = true;
    document.getElementById("chancesPlusBtn").disabled = true;
    document.getElementById("chancesResetBtn").disabled = true;

    let oldWidth = $("#qpOptionContainer").width();
    $("#qpOptionContainer").width(oldWidth + 35);
    $("#qpOptionContainer > div").append(counterWindowOpenButton);

    counterWindow.body.attr("id", "counterWindowBody");
}